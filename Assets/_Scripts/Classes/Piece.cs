﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public enum PieceColor
{
	WHITE, BLACK
}

public enum PieceType
{
	PAWN, ROOK, KNIGHT, BISHOP, QUEEN, KING	
}

public struct PossibleMove
{
	public Vector2 position;
	public MoveType type;

	public PossibleMove(Vector2 position, MoveType type)
	{
		this.position = position;
		this.type = type;
	}
}

public class Piece
{
	public Vector2 initial;	// The Initial Postion of The Piece
	public Vector2 position; // position of the Piece on the Board
	public PieceColor color; // Color Of the Piece
	public PieceType type;	// Type Of Piece

	Board board;   // Stores the Parent Board 

	public Piece(Board board, PieceColor color, PieceType type)
	{
		this.color = color;
		this.type = type;
		this.board = board;
	}

	public Piece(Piece piece)
	{
		this.color = piece.color;
		this.type = piece.type;
		this.board = piece.board;
		this.initial = piece.initial;
		this.position = piece.position;
	}

	public override string ToString ()
	{
		return (color.ToString () + type.ToString ()).ToLower();
	}

	public List<PossibleMove> GetPossibleMoves()
	{
		switch (type)
		{
			case PieceType.PAWN:
				return PawnPossibleMoves();
			case PieceType.KNIGHT:
				return KnightPossibleMoves();
			case PieceType.ROOK:
				return RookPossibleMoves();
			case PieceType.BISHOP:
				return BishopPossibleMoves();
			case PieceType.QUEEN:
				return QueenPossibleMoves();
			case PieceType.KING:
				return KingPossibleMoves();
			default:
				return null;
		}
	}

	// Explore The Moves of a Pawn
	List<PossibleMove> PawnPossibleMoves()
	{
		var moves = new List<PossibleMove> ();

		// Check For Possible Moves 
		if (color == PieceColor.WHITE) 
		{
			// Check One Step Ahead
			Vector2 next = position + new Vector2(0,1);
			if (board.InBounds(next) && board.IsEmpty(next))
			{
				moves.Add(new PossibleMove(next, MoveType.MOVE));
				// Check if it's the First Move
				if (position.y == 1)
				{
					// Check Two Steps Ahead
					Vector2 next2 = position + new Vector2(0,2);
					if (board.InBounds(next2) && board.IsEmpty(next2))
					{
						moves.Add(new PossibleMove(next2, MoveType.MOVE));
					}
				} 
			}
			
			// Check For Possible Attacks
			Vector2 attack1 = position + new Vector2(1,1);
			Vector2 attack2 = position + new Vector2(-1,1);
			if (board.InBounds(attack1) )
			{
				if (board.IsOfColor(attack1, PieceColor.BLACK))
				{
					moves.Add(new PossibleMove(attack1, MoveType.CAPTURE));
				}
				else // Check For EnPassant
				{
					if (position.y == 4 && board.IsEmpty(attack1))
					{
						Vector2 captured1 = attack1 + new Vector2(0,-1);
						Piece piece1 = board.GetPiece(captured1);
						if (piece1 != null && piece1.color == PieceColor.BLACK 
						    && piece1.type == PieceType.PAWN)
						{
							moves.Add(new PossibleMove(attack1, MoveType.CAPTURE));
						}
					}
				}
			}
			if (board.InBounds(attack2))
			{
				if (board.IsOfColor(attack2, PieceColor.BLACK))
				{
					moves.Add(new PossibleMove(attack2, MoveType.CAPTURE));
				}
				else
				{
					if (position.y == 4 && board.IsEmpty(attack2))
					{
						Vector2 captured2 = attack2 + new Vector2(0,-1);

						Piece piece2 = board.GetPiece(captured2);
						if (piece2 != null && piece2.color == PieceColor.BLACK 
						    && piece2.type == PieceType.PAWN)
						{
							moves.Add(new PossibleMove(attack2, MoveType.CAPTURE));
						}
					}
				}
			}
		
		}
		else // BLACK PAWN
		{
			// Check One Step Ahead
			Vector2 next = position + new Vector2(0,-1);
			if (board.InBounds(next) && board.IsEmpty(next))
			{
				moves.Add(new PossibleMove(next, MoveType.MOVE));
				// Check if it's the First Move
				if (position.y == 6)
				{
					// Check Two Steps Ahead
					Vector2 next2 = position + new Vector2(0,-2);
					if (board.InBounds(next2) && board.IsEmpty(next2))
					{
						moves.Add(new PossibleMove(next2, MoveType.MOVE));
					}
				}
			}
			
			// Check For Possible Attacks
			Vector2 attack1 = position + new Vector2(1,-1);
			Vector2 attack2 = position + new Vector2(-1,-1);
			if (board.InBounds(attack1))
			{
				if (board.IsOfColor(attack1, PieceColor.WHITE))
				{
					moves.Add(new PossibleMove(attack1, MoveType.CAPTURE));
				}
				else // Check for EnPassant
				{
					if (position.y == 3 && board.IsEmpty(attack1))
					{
						Vector2 captured1 = attack1 + new Vector2(0,1);
						Piece piece1 = board.GetPiece(captured1);
						if (piece1 != null && piece1.color == PieceColor.WHITE 
						    && piece1.type == PieceType.PAWN)
						{
							moves.Add(new PossibleMove(attack1, MoveType.CAPTURE));
						}
					}
				}
			}
			if (board.InBounds(attack2))
			{
				if (board.IsOfColor(attack2, PieceColor.WHITE))
				{
					moves.Add(new PossibleMove(attack2, MoveType.CAPTURE));
				}
				else // Check for EnPassant
				{
					if (position.y == 3 && board.IsEmpty(attack2))
					{
						Vector2 captured2 = attack2 + new Vector2(0,1);
						Piece piece2 = board.GetPiece(captured2);
						if (piece2 != null && piece2.color == PieceColor.WHITE 
						    && piece2.type == PieceType.PAWN)
						{
							moves.Add(new PossibleMove(attack2, MoveType.CAPTURE));
						}
					}
				}
			}
		}

		return moves;
	}

	// Get The Possible Moves of a Knight
	List<PossibleMove> KnightPossibleMoves()
	{
		var moves = new List<PossibleMove> ();
		
		var enemyColor = (color == PieceColor.BLACK) ?
			PieceColor.WHITE : PieceColor.BLACK;
		
		// Explore the moves of a Knight (L shape)
		Vector2[] jumps = { new Vector2(1,2), new Vector2(2,1),
			new Vector2(-1,2), new Vector2(2,-1),
			new Vector2(1,-2), new Vector2(-2,1),
			new Vector2(-1,-2), new Vector2(-2,-1)
		};
		
		foreach (var jump in jumps)
		{
			Vector2 next = position + jump;
			if (board.InBounds(next))
			{
				if (board.IsEmpty(next))
				{	// Add a Possible Move
					moves.Add(new PossibleMove(next, MoveType.MOVE));
				}
				else if (board.IsOfColor(next, enemyColor))
				{	// Add a Possible Attack
					moves.Add(new PossibleMove(next, MoveType.CAPTURE));
				}
			}
		}
		
		return moves;
	}

	// Get The Possible Moves of a Rook
	List<PossibleMove> RookPossibleMoves()
	{
		var moves = new List<PossibleMove> ();

		// Explore Moves In the 4 Lateral and Horizontal Directions
		moves.AddRange(ExploreDirection (new Vector2 (1, 0)));
		moves.AddRange(ExploreDirection (new Vector2 (-1, 0)));
		moves.AddRange(ExploreDirection (new Vector2 (0, 1)));
		moves.AddRange(ExploreDirection (new Vector2(0,-1))); 
				
		return moves;
	}

	// Get The Possible Moves of a Bishop
	List<PossibleMove> BishopPossibleMoves()
	{
		var moves = new List<PossibleMove> ();
		
		// Explore Moves In the Diagonal Directions
		moves.AddRange(ExploreDirection ( new Vector2 (1, 1)));
		moves.AddRange(ExploreDirection (new Vector2 (-1, 1)));
		moves.AddRange(ExploreDirection ( new Vector2 (1, -1)));
		moves.AddRange(ExploreDirection ( new Vector2(-1,-1))); 

		return moves;
	}

	// Get The Possible Moves of a Queen
	List<PossibleMove> QueenPossibleMoves()
	{
		var moves = new List<PossibleMove> ();
		
		// Explore Moves In the 4 Lateral and Horizontal Directions
		moves.AddRange(ExploreDirection (new Vector2 (1, 0)));
		moves.AddRange(ExploreDirection (new Vector2 (-1, 0)));
		moves.AddRange(ExploreDirection (new Vector2 (0, 1)));
		moves.AddRange(ExploreDirection (new Vector2(0,-1))); 

		// Explore Moves In the Diagonal Directions
		moves.AddRange(ExploreDirection ( new Vector2 (1, 1)));
		moves.AddRange(ExploreDirection (new Vector2 (-1, 1)));
		moves.AddRange(ExploreDirection ( new Vector2 (1, -1)));
		moves.AddRange(ExploreDirection ( new Vector2(-1,-1))); 
		
		return moves;
	}

	// Get The Possible Moves of a King
	List<PossibleMove> KingPossibleMoves()
	{
		var moves = new List<PossibleMove> ();

		var enemyColor = (color == PieceColor.BLACK) ?
			PieceColor.WHITE : PieceColor.BLACK;
		
		// Explore One Step in All Directions
		Vector2[] steps = { new Vector2(-1,-1), new Vector2(0,-1),
			new Vector2(1,-1), new Vector2(1,0),
			new Vector2(1,1), new Vector2(0,1),
			new Vector2(-1,1), new Vector2(-1,0)
		};
		
		foreach (var step in steps)
		{
			Vector2 next = position + step;
			if (board.InBounds(next) )
			{
				if (board.IsEmpty(next))
				{	// Add a Possible Move
					moves.Add (new PossibleMove(next, MoveType.MOVE));
				}
				else if (board.IsOfColor(next, enemyColor))
				{	// Add a Possible Attack
					moves.Add(new PossibleMove(next, MoveType.CAPTURE));
				}
			}
		}

		// Check for Castling
		moves.AddRange (ExploreCasteling());

		return moves;
	}

	// Checks for Casteling Sides
	List<PossibleMove> ExploreCasteling()
	{
		var moves = new List<PossibleMove> ();

		// If For The King of 'color' is its initial Position
		if ((color == PieceColor.WHITE && position == new Vector2(4,0))
		    || (color == PieceColor.BLACK && position == new Vector2(4,7)))
		{
			// Get The Row of the king
			float row = position.y;
			
			// Check for King Side Castle
			if (board.IsEmpty(new Vector2(5, row))
			    && board.IsEmpty(new Vector2(6, row)))
			{
				// Check if The Last Corner has a Rook of the same color
				var piece = board.GetPiece(new Vector2(7,row));
				if (piece != null && piece.color == color && piece.type == PieceType.ROOK)
				{
					moves.Add(new PossibleMove(new Vector2(6, row), MoveType.MOVE));
				}
			}
			
			// Check for Queen Side Castle
			if (board.IsEmpty(new Vector2(3, row))
			    && board.IsEmpty(new Vector2(2, row))
			    && board.IsEmpty(new Vector2(1, row)))
			{
				// Check if The Last Corner has a Rook of the same color
				var piece = board.GetPiece(new Vector2(0,row));
				if (piece != null && piece.color == color && piece.type == PieceType.ROOK)
				{
					moves.Add(new PossibleMove(new Vector2(2, row), MoveType.MOVE));
				}
			}
		}

		return moves;
	}

	// Explores The Board Squares from this.position follwing a Direction
	List<PossibleMove> ExploreDirection( Vector2 direction)
	{
		var moves = new List<PossibleMove>();
		// Get Enemys Color
		PieceColor enemyColor = (color == PieceColor.WHITE) ? 
			PieceColor.BLACK : PieceColor.WHITE;
		// Get The First Next Position
		Vector2 next = position + direction;
		
		// While The Next Position is in The Borads Bounds
		while (board.InBounds(next)) 
		{
			if (board.IsEmpty(next))
			{	// Add a Possible Move
				moves.Add (new PossibleMove(next, MoveType.MOVE));
				next = next + direction;
			}
			else if (board.IsOfColor(next, enemyColor))
			{	// Add a Possible Attack
				moves.Add (new PossibleMove(next, MoveType.CAPTURE));
				// The Piece cannot Move Further
				break;
			}
			else
			{
				// There is a Friendy Piece Blocking the way
				// The Piece cannot Move Further
				break;
			}
		}
		
		return moves;
	}
}
