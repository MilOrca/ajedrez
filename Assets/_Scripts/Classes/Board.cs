﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public enum MoveType
{
	NONE, MOVE, CAPTURE, SELECTED
}

public enum CastlingSide
{
	NONE, KING_SIDE, QUEEN_SIDE
}

public struct MoveInfo
{
	public MoveType type;
	public Vector2 from, to, captured;
	public bool check;
	public bool checkMate;
	public PieceType promotedTo;
	public CastlingSide castling;
	public string notation;
}

public class Board
{
	public Piece[,] positions; // positions on board (has Piece or null)
	public PieceColor playerColor; // Indicates whitch color The Player has	
	public Stack<Piece> capturedPieces; // Stores Captured Pieces
	public Piece selectedPiece;  // Stores the selected Piece

	public bool CastleMove; // Flag To Indicate The Next Move is a Castling
	public MoveInfo RookMove; // The Corresponding Rook Move to The Castle

	public bool Promotion; // Flag To Indicate Promotion
	public PieceType PromotedTo; // Indicates The Promoted To Peice

	Board 	boardImage; // To Store the Initial State of The Board
	public PieceColor currentColor; // Indicates Turn of whitch color 

	// Final Moves to solve the game
	int finalIndex;
	List<MoveInfo> finalMoves;
	public int turn;
	
	public Board()
	{
		positions = new Piece[8, 8];
		capturedPieces = new Stack<Piece> ();
		finalMoves = new List<MoveInfo> ();
		finalIndex = 0;
		turn = 1;
	}

	// Set the Color of Player
	public void  SetPlayersColor(PieceColor color)
	{
		playerColor = color;
		currentColor = color;
	}

	// Add a Piece at the (x,y) position
	public void AddPiece(int x, int y, PieceColor color, PieceType type)
	{
		Piece piece = new Piece (this, color, type);
		piece.initial = new Vector2 (x, y);
		piece.position = new Vector2 (x, y);
		positions [y, x] = piece;
	}

	// Adds a Fianl Move
	public void AddFinalMove(MoveInfo move)
	{
		finalMoves.Add (move);
	}

	// Returns A Piece by it's Position
	public Piece GetPiece(Vector2 position)
	{
		return positions [(int)position.y, (int)position.x];
	}

	// Get The Pieces on Board
	public Piece[] GetPieces()
	{
		List<Piece> pieces = new List<Piece>();
		for (int i=0; i<8; i++)
		{
			for (int j=0; j<8; j++)
			{
				if (positions[i,j] != null)
				{
					pieces.Add(positions[i,j]);
				}
			}
		}
		return pieces.ToArray ();
	}

	// Save Board State
	public void SaveBoardSatate()
	{
		boardImage = new Board ();

		boardImage.positions = new Piece[8,8];
		for (int i=0; i<8; i++)
		{
			for (int j=0; j<8; j++)
			{
				if (positions[i,j] != null)
				{
					boardImage.positions[i,j] = new Piece( positions[i,j]);
				}
			}
		}

		boardImage.playerColor = playerColor;
		boardImage.capturedPieces = capturedPieces; 
		boardImage.selectedPiece = selectedPiece; 

		boardImage.CastleMove = CastleMove;
		boardImage.RookMove = RookMove;

		boardImage.currentColor = currentColor;
		
		boardImage.finalIndex = finalIndex;
		boardImage.finalMoves = finalMoves;
		boardImage.turn = turn;
	}

	// Reset The Board to Its Initial State
	public void Reset()
	{
		if (boardImage != null) 
		{
			positions = boardImage.positions; 
			playerColor = boardImage.playerColor;
			capturedPieces = boardImage.capturedPieces; 
			selectedPiece = boardImage.selectedPiece; 
		
			CastleMove = boardImage.CastleMove;
			RookMove = boardImage.RookMove;

			currentColor = boardImage.currentColor;

			finalIndex = boardImage.finalIndex;
			finalMoves = boardImage.finalMoves;
			turn = boardImage.turn;

			var pieces = GetPieces();
			foreach(var piece in pieces)
			{
				piece.position = piece.initial;
			}
		}
	}

	// Process The Possible Moves of a Piece in a given Position
	public List<PossibleMove>  GetPossibleMoves(Vector2 position)
	{
		// Get Selected Piece
		Piece selected = positions [(int)position.y, (int)position.x];
		if (selected != null && selected.color == playerColor)
		{
			// Get Possible Moves Of the Piece
			var moves = selected.GetPossibleMoves();
			if (moves.Count > 0)
			{
				// Save The Selected Piece
				selectedPiece = selected;
				return moves;
			}
		}

		return null;
	}

	// Checks if The Next Move if for the opponent
	public bool IsPlayersTrun()
	{
		return playerColor == currentColor;
	}

	// Checks if There's No further moves (Solved)
	public bool IsSolved()
	{
		return finalIndex == finalMoves.Count;
	}
	
	// Checks if a position is in The Bounds of the Board
	public bool InBounds(Vector2 position)
	{
		return (position.x >= 0 && position.x < 8) 
			&& (position.y >= 0 && position.y < 8);
	}
	
	// Checks if a given position holds a piece of 'color'
	public bool IsOfColor(Vector2 position, PieceColor color)
	{
		Piece piece = positions [(int)position.y, (int)position.x];
		if (piece != null)
		{
			return piece.color == color;
		}
		
		return false;
	}

	// Checks if a position is Empty
	public bool IsEmpty(Vector2 position)
	{
		return positions [(int)position.y, (int)position.x] == null;
	}

	// Gets The Next Move, Usualy for The Opponent
	public MoveInfo GetNextMove()
	{
		if (finalIndex < finalMoves.Count) 
		{
			// Get the Next final Move
			var final = finalMoves [finalIndex];
			// Apply It
			ApplyMove(final);
			return final;
		}
		// Return None Move
		MoveInfo move = new MoveInfo();
		move.type = MoveType.NONE;
		return move;
	}

	// Switch Current Colors Turn
	public void SwitchTurn()
	{
		currentColor = (currentColor == PieceColor.WHITE) ? 
			PieceColor.BLACK : PieceColor.WHITE;
	}

	// Returns a Hint On The Next Piece to Move
	public Vector2 Hint()
	{
		Vector2 from = new Vector2 (-1, -1);
		if (finalIndex < finalMoves.Count)
		{
			from = finalMoves[finalIndex].from;
		}
		return from;
	}

	// Special Moves
	//
	//   Castling :
	//		1) Naither Rook Nor King have been Moved
	//		2) All Sqaures Between King and Rook are Empty
	//		3) The King must not be in Check befor or after the Castling
	//		=> The King Mmoves 2 Sqaures Towas The Rook, 
	//			and The Rook Jumps Over The King
	//
	//	 Pawn Promotion
	//		1) Pawn arrives to the Other Side of the Board
	//		=> Pawn can be nay of: Knight, Rook, Bishop and Queen
	//
	//	 En Passant
	//		1) Pawn has advanced 3 Squares
	//		2) Enemy Pawn had immidiatly made First Move with 2 Sqaures
	//		3) Enemy Pawn had landed the left or right side of the Pawn
	//		=> Pawn Advances behind the Enemy Pawn
	//			and Enemy Pawn gets Eleminated
	//

	// Check if Next Move Coincide With The Next Final Move
	public MoveInfo CheckMove(Vector2 to)
	{
		MoveInfo info = new MoveInfo();
		// Get The Next final Move
		MoveInfo final = finalMoves [finalIndex];
		// Check The origin and Destination coincide
		if ( final.from == selectedPiece.position
			&& final.to == to) {
			info = final;
			// Apply the Move
			ApplyMove(info);
		}
		else
		{
			// Retun None Move
			info.type = MoveType.NONE;
		}

		return info;
	}

	// Makes The Necesary Changes by Moving, Capturing The Pieces on the Board
	void ApplyMove(MoveInfo move)
	{
		// If It's Castling
		if (move.castling != CastlingSide.NONE)
		{
			RookMove = ApplyCastling(move);
			CastleMove = true;
		} 
		else
		{
			// If There's a capture
			if (move.captured.x != -1)
			{
				Vector2 pos = move.captured;
				// Add Captured Piece to the Stack
				var captured = GetPiece (pos);
				capturedPieces.Push (captured);
				// Remove the captured piece from the Board
				positions [(int)pos.y, (int)pos.x] = null;
			}

			// Move The Selected Piece
			Piece selectedPiece = positions [(int)move.from.y, (int)move.from.x]; 
			selectedPiece.position = move.to;
			positions [(int)move.to.y, (int)move.to.x] = selectedPiece;
			positions [(int)move.from.y, (int)move.from.x] = null;

			// Increment turn if it's Black
			if (selectedPiece.color == PieceColor.BLACK)
			{
				turn++;
			}

			// If It's a Pawn With Promotion
			if (move.promotedTo != PieceType.PAWN)
			{
				selectedPiece.type = move.promotedTo;
				Promotion = true;
				PromotedTo = move.promotedTo;
			}
		}

		// Point to The Next Final Move
		finalIndex++;
	}

	// Makes The Necesary Changes to make a castling
	MoveInfo ApplyCastling(MoveInfo move)
	{
		// Move The King
		Piece king = positions [(int)move.from.y, (int)move.from.x]; 
		king.position = move.to;
		positions [(int)move.to.y, (int)move.to.x] = king;
		positions [(int)move.from.y, (int)move.from.x] = null;

		// Get 'from', 'to' Position based on The Casteling Side
		Vector2 from = new Vector2 ();
		Vector2 to = new Vector2();
		// Same Row as The King
		from.y = move.from.y;
		to.y = move.to.y;
		if (move.castling == CastlingSide.KING_SIDE) 
		{
			from.x = 7;
			to.x = 5;
		}
		else
		{
			from.x = 0;
			to.x = 3;
		}

		// Move the Rook
		Piece rook = positions [(int)from.y, (int)from.x]; 
		rook.position = to;
		positions [(int)to.y, (int)to.x] = rook;
		positions [(int)from.y, (int)from.x] = null;

		// Return The Move For Animation
		MoveInfo rookMove = move;
		rookMove.from = from;
		rookMove.to = to;

		return rookMove;
	}
	
}
