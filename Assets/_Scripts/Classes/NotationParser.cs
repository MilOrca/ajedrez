﻿using UnityEngine;
using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;

public static class NotationParser
{
	// The Board To Parse and Return on Load
	static Board board;

	// Loads and Parses The Notarions from 'filePath'
	public static Board Load(string notation)
	{
		board = new Board ();

		// Split the content of the file into Lines
		string[] lines = notation.Split('\n');
		// Line counter
		int count = 0;
		try
		{
			// Parse The none Empty Lines to Moves
			foreach (string line in lines) 
			{
				string refLine = line.ToString();
				// Skip Empty Spaes and Tabs
				if (!string.IsNullOrEmpty(refLine) && SkipEmpty (ref refLine))
				{
					ParseLine(refLine);
				}
				count++;
			}
		}
		catch (Exception e)
		{
			string line = (count < lines.Length ) ? lines[count] : "";
			Debug.LogError(string.Format("Line {0} - {1} : {2}",
			                             count, line, e.Message));
		}
		// Reset To Initial State
		board.Reset ();
		return board;
	}

	// Parse a single Line
	static void ParseLine(string line)
	{
		// Clone the original Line for reading and manipulation
		string cline = line.ToString();
		// Read Turn  (First Digits)
		int turn = ReadTurn (ref cline);
		// Read Color (First Dots)
		PieceColor color = ReadColor(ref cline);
		// Check if It's an Initial Position or a Move
		if (turn == 0)
		{
			// Read InitialPosition
			ReadInitialPosition(color, ref cline);
		}
		else // It's a Move
		{
			if (turn == 1)
			{
				// Set First Color move to The Player
				board.SetPlayersColor(color);
				// Save The Initial State, For later Reset
				board.SaveBoardSatate();
			}

			// Skip Empty Spaes and Tabs
			SkipEmpty (ref cline);
			// Read First String to Parse a move
			string firstMove = ReadMove(ref cline);
			MoveInfo move = ParseMove( color, firstMove);
			board.AddFinalMove(move);
			// Apply Temporary Move
			board.GetNextMove();

			// Check if there is a second move (only black)
			if (SkipEmpty (ref cline))
			{
				MoveInfo move2 = ParseMove(PieceColor.BLACK, cline);
				board.AddFinalMove( move2);
				// Apply Temporary Move
				board.GetNextMove();
			}
		}
	}

	// Read The Initial Postion of the pieces (Turn 0)
	static void ReadInitialPosition(PieceColor color, ref string line)
	{
		// Read Type
		PieceType type = ReadType (ref line);
		// Read Position
		Vector2 to = ReadDestination (ref line);
		// Read 
		board.AddPiece ((int)to.x, (int)to.y, color, type); 
	}

	// Parse a Line to MoveInfo
	static MoveInfo ParseMove (PieceColor color, string line)
	{
		MoveInfo move;
		move.notation = line;

		PieceType type;
		// Parametres of The Move
		MoveType moveType = MoveType.MOVE;
		Vector2 from, to, captured = new Vector2(-1,-1);
		bool check, checkMate;
		PieceType promotedTo;
		CastlingSide castling = CastlingSide.NONE;

		// Check if there's Check or CheckMate
		check = ReadCheck (ref line);
		checkMate = ReadCheckMate (ref line);

		// Check if it's a Castling
		bool kingSide;
		if (ReadCastling (ref line, out kingSide)) 
		{
			// Based on color and Castling side, assign Kings 'from' and 'to'
			type = PieceType.KING;
			GetCastlingPosition(color, kingSide, out from, out to);
			promotedTo = PieceType.PAWN;
			castling = (kingSide) ? CastlingSide.KING_SIDE : CastlingSide.QUEEN_SIDE;
		}
		else
		{
			// Read Piece Type
			type = ReadType (ref line);
			// Read Move Destination
			to = ReadDestination (ref line);
			// Check for capture
			if (ReadCapture (ref line))
			{
				moveType = MoveType.CAPTURE;

				// Check In Case of EnPassant
				if (type == PieceType.PAWN &&  board.IsEmpty(to))
				{
					int row = (color == PieceColor.WHITE) ? (int)to.y -1 : (int)to.y + 1;
					captured = new Vector2(to.x, row);
					// Validate the EnPassant Move
					IsEnPassant(color, captured);
				}
				else
				{
					// Normal Capture
					captured = to; 
				}
			}

			promotedTo = (type == PieceType.PAWN) ?
				ReadPromotion (ref line) : PieceType.PAWN;
			from = GetOrigin (color, type, line, to);
		}

		move.type = moveType;
		move.from = from;
		move.to = to;
		move.captured = captured;
		move.check = check;
		move.checkMate = checkMate;
		move.promotedTo = promotedTo;
		move.castling = castling;

		return move;
	}

    // Check for possible En Passant
 	static void IsEnPassant(PieceColor color, Vector2 captured)
	{
		// Check The Correct Row 
		if ((color == PieceColor.WHITE && captured.y == 4) 
		    || (color == PieceColor.BLACK && captured.y == 3))
		{
			// Check the Captured Piece
			var piece = board.GetPiece(captured);
			var enemyColor = (color == PieceColor.WHITE) ? PieceColor.BLACK : PieceColor.WHITE;
			if (piece != null && piece.type == PieceType.PAWN && piece.color == enemyColor)
			{
				return;
			}
		}

		throw new Exception(string.Format(
			"Invalid En Passant Move: color {0}, position ({1}, {2})",
			color, captured.x, captured.y));
	}

	// Read The Turn 
	static int ReadTurn(ref string line)
	{
		// Accumulate all the first digits
		string digits = "";
		while (char.IsDigit(line[0]))
		{
			digits += line[0];
			line = line.Remove(0,1);
		}
		// Try Parse the digits to integer 
		int turn;
		if (int.TryParse (digits, out turn)) 
		{
			return turn;
		}
		// Format Exception 
		throw new Exception (
			string.Format ("Error at reading Turn Number : ({0})", digits));
	}

	// Read the Color
	static PieceColor ReadColor(ref string line)
	{
		// Skip all first spaces
		while (line[0] == ' ')
		{
			line = line.Remove(0,1);
		}
		// Accumulate Next Dots
		string dots = "";
		while (line[0] == '.')
		{
			dots += '.';
			line = line.Remove(0,1);
		}
		// Try Parse the dots to Color
		if (dots.Equals (".")) 
		{
			return PieceColor.WHITE;
		} 
		else if (dots.Equals ("...")) 
		{
			return PieceColor.BLACK;
		}
		// Format Exception 
		throw new Exception (
			string.Format ("Error at reading Color : ({0})", dots));
	}

	// Read the Piece, Move, Capture, ...
	static string ReadMove(ref string line)
	{
		// Skip all first spaces
		while (line[0] == ' ')
		{
			line = line.Remove(0,1);
		}
		// Read The Move
		string move = "";
		while (line.Length > 0 && line[0] != ' ' && line[0] != '\t') 
		{
			move += line[0];
			line = line.Remove(0,1);
		}
		return move;
	}

	// Checks if there's a second move to read
	static bool SkipEmpty(ref string line)
	{
		// skip tabs and spaces
		while(line.Length > 0 && (line[0] == ' ' || line[0] == '\t'))
		{
			line = line.Remove(0,1);
		}
		// check for end of line '\n'
		return line.Length > 0;
	}

	// Parse The Piece Type from the first letter
	static PieceType ReadType(ref string move)
	{
		// If there was no Upper case, return PAWN
		if (!char.IsUpper (move [0]))
		{
			return PieceType.PAWN;
		}

		// Check the Type from first Letter
		PieceType type;
		switch (move [0])
		{
		case 'N':
			type = PieceType.KNIGHT;
			break;
		case 'R':
			type = PieceType.ROOK;
			break;
		case 'B':
			type = PieceType.BISHOP;
			break;
		case 'Q':
			type = PieceType.QUEEN;
			break;
		case 'K':
			type = PieceType.KING;
			break;
		default:
			throw new Exception("Error at Parsing Piece Type");
		}

		move = move.Remove (0,1);
		return type;
	}

	// Parse Castling, and returns true if there's a castling
	static bool ReadCastling(ref string move, out bool kingSide)
	{
		// Accumulate '0' and '-'
		string chars = "";
		int i = 0;
		while (i < move.Length &&
		       (move[i] == '0' || move[i] == '-' || move[i] == 'O'))
		{
			chars += move[i];
			i++;
		}

		kingSide = false;
		if (chars.Equals ("0-0") || chars.Equals("O-O")) 
		{
			kingSide = true;
			move = move.Remove(0,3);
		} 
		else if (chars.Equals ("0-0-0")|| chars.Equals("O-O-O"))
		{
			kingSide = false;
			move = move.Remove(0,5);
		}
		else
		{
			return false;
		}

		return true;
	}

	// Pasre The Destinacion of the move
	static Vector2 ReadDestination(ref string move)
	{
		int len = move.Length;
		// Get The offset based on the last digit
		int offset = len-2;
		while (offset > -1 && !char.IsLower(move[offset])) 
		{
			offset--;
		}
		// Check the value of offset
		if (offset > -1) 
		{
			// Get Column and Row
			int y = ParseRow(move[offset+1]);
			int x = ParseColumn(move[offset]);
			move = move.Remove(offset, 2);
			return new Vector2(x,y);
		}

		// In case of error
		throw new Exception (
			string.Format("Parsing Destination Square ({0})", move));
	}

	// Parse if there's a Capture
	static bool ReadCapture(ref string line)
	{
		int index = line.IndexOf ('x');
		if (index > -1) 
		{
			line = line.Remove(index, 1);
			return true;
		}
		return false;
	}

	// Parse if there's a Check
	static bool ReadCheck(ref string line)
	{
		int index = line.IndexOf ('+');
		if (index > -1) 
		{
			line = line.Remove(index, 1);
			return true;
		}
		return false;
	}

	// Parse if there's a Check Mate
	static bool ReadCheckMate(ref string line)
	{
		int index = line.IndexOf ('#');
		if (index > -1) 
		{
			line = line.Remove(index, 1);
			return true;
		}
		return false;
	}

	// Parse the Promoted To Piece 
	static PieceType ReadPromotion(ref string line)
	{
		PieceType newPiece = PieceType.PAWN;
		if (line.Length > 0)
		{
			char last = line[line.Length - 1];
			switch(last)
			{
				case 'N':
					newPiece = PieceType.KNIGHT;
					break;
				case 'R':
					newPiece = PieceType.ROOK;
					break;
				case 'B':
					newPiece = PieceType.BISHOP;
					break;
				case 'Q':
					newPiece = PieceType.QUEEN;
					break;
				default:
					return PieceType.PAWN;
			}

			line = line.Remove(line.Length -1, 1);
		}
		return newPiece;
	}

	// Gets The Original Position of The Move, and Resolve Ambiguity if necessary
	static Vector2 GetOrigin(PieceColor color, PieceType type, 
	                         string origin, Vector2 destination)
	{
		// Provisional Position by Column, Row or Both
		Vector2 initial = new Vector2(-1,-1);
		foreach (char o in origin)
		{
			if (char.IsDigit(o))
			{
				initial.y = ParseRow(o);
			}
			else if (char.IsLower(o))
			{
				initial.x = ParseColumn(o);
			}
			else if (o == ' ')
			{
				continue;
			}
			else
			{
				throw new Exception(
					string.Format("Parsing The Original Position ({0})", origin));
			}
		}

		// Get Pieces with same Color and Type
		var pieces = GetPiecesWith (color, type);
		// Filter Whitch Piece can Move to position
		var possiblePieces = GetPiecesCanMoveTo (pieces, destination);
		if (possiblePieces.Length == 1)
		{
			return possiblePieces[0].position;
		}
		else
		{
			// Resolve Ambiguity
			return ResolvePosition (possiblePieces, initial);
		}
	}

	// Get the Pieces that match Color and Type
	static Piece[] GetPiecesWith (PieceColor color, PieceType type)
	{
		var pieces = board.GetPieces ().Where (
			p => p.type == type && p.color == color).ToArray ();
		if (pieces.Length == 0)
		{
			throw new Exception(
				string.Format("There's No {0} Pieces of Type {1}", color, type));
		}
		return pieces;
	}

	// Gets The Pieces That Can move to destination
	static Piece[] GetPiecesCanMoveTo(Piece[] pieces, Vector2 destination)
	{
		List<Piece> canMoveTo = new List<Piece> ();

		foreach(var piece in pieces)
		{
			// Check if destination is in The Possible Moves of the Piece
			var moves = piece.GetPossibleMoves();
			if ( moves.Count(m => m.position == destination) > 0)
			{
				canMoveTo.Add(piece);
			}
		}

		// Check For Error
		if (canMoveTo.Count == 0)
		{
			throw new Exception(string.Format(
				"No Piece Can Move to ({0}, {1})", destination.x, destination.y));
		}

		return canMoveTo.ToArray ();
	}

	// Get the Final Orginal Position
	static Vector2 ResolvePosition (Piece[] pieces, Vector2 initial)
	{
		Vector2 origin;
		if (pieces.Length == 1) // No Ambiguity
		{
			origin = pieces [0].position;
		}
		else // Resolve Ambiguity
		{
			Piece[] resolve;
			if (initial.x != -1)
			{ // Resolve by Column
				resolve = pieces.Where(p => p.position.x == initial.x ).ToArray();
			}
			else if (initial.y != -1) 
			{ // Resolve by Row
				resolve = pieces.Where(p => p.position.y == initial.y ).ToArray();
			} 
			else
			{ // Resolve By Both
				resolve = pieces.Where(p => p.position == initial ).ToArray();
			}

			// Check if Ambiguity Was Resolved
			if (resolve.Length == 1)
			{
				origin = resolve[0].position;
			}
			else
			{
				throw new Exception(string.Format(
					"Ambiguity UnResolved, Nº possible Cases : {0}", resolve.Length));
			}
		}

		return origin;
	}

	// Get The 'from', 'to' positions of the king for Castling
	static void GetCastlingPosition(PieceColor color, bool kingSide, 
	                          out Vector2 from, out Vector2 to)
	{
		from = new Vector2 ();
		to = new Vector2 ();

		// The Two Kings have The Sam Initial Column
		from.x = 4;

		if (color == PieceColor.WHITE)
		{
			from.y = to.y = 0; // First Row of White
		}
		else
		{
			from.y = to.y = 7; // First Row of Black
		}
		// King Side 'g' Column, Queen Side 'c' Column
		to.x = (kingSide) ? 6 : 2;
	}

	// Parse The Column from a Letter
	static int ParseColumn(char column)
	{
		int ascii = (int)column - 97;
		if (ascii >= 0 && ascii < 8)
		{
			return ascii;
		}

		throw new Exception(
			string.Format("Parsing The Column ({0})", column));
	}

	// Parse The Column from a Letter
	static int ParseRow(char row)
	{
		int ascii = (int)row - 49;
		if (ascii >= 0 && ascii < 8)
		{
			return ascii;
		}
		
		throw new Exception(
			string.Format("Parsing The Row ({0})", row));
	}
}
