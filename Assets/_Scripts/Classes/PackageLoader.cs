﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Xml;
using System.Text;

public struct Level {
	public string ID, Event, Site, Date, Round, White, Black, Result, Play;

	public Level(XmlNode levelNode)
	{
		ID = levelNode.Attributes["id"].Value;
		Event = levelNode.Attributes["event"].Value;
		Site = levelNode.Attributes["site"].Value;
		Date = levelNode.Attributes["date"].Value;
		Round = levelNode.Attributes["round"].Value;
		White = levelNode.Attributes["white"].Value;
		Black = levelNode.Attributes["black"].Value;
		Result = levelNode.Attributes["result"].Value;
		Play = levelNode.InnerText;
	}

	public override string ToString()
	{
		StringBuilder builder = new StringBuilder ();
		builder.Append ("Event : " + Event + "\n");
		builder.Append ("Site : " + Site + "\n");
		builder.Append ("Date : " + Date + "\n");
		builder.Append ("Round : " + Round + "\n");
		builder.Append ("White : " + White + "\n");
		builder.Append ("Black : " + Black + "\n");
		builder.Append ("Result : " + Result + "\n");

		return builder.ToString ();
	}
}

public class Package {
	public readonly string name;
	public List<Level> levels;

	public Package(XmlNode packageNode)
	{
		name = packageNode.Attributes["name"].Value;

		levels = new List<Level> ();
		foreach (XmlNode node in packageNode.ChildNodes)
		{
			levels.Add(new Level(node));
		}
	}
}

public static class PackageLoader
{
	public static List<Package> LoadLocal(string path)
	{
		TextAsset xml = Resources.Load(path) as TextAsset;

		return ParseXml(xml.text);
	}

	public static List<Package> Download(string url)
	{
		WWW www = new WWW (url);
		while (!www.isDone) 
		{
		}
		return ParseXml (www.text);
	}

	static List<Package> ParseXml(string xmlText)
	{
		XmlDocument xml = new XmlDocument ();
		xml.LoadXml (xmlText);
		
		var packageNodes = xml.GetElementsByTagName("package");
		var packages = new List<Package> ();

		foreach(XmlNode node in packageNodes)
		{
			packages.Add(new Package(node));
		}
		
		
		return packages;
	}
}
