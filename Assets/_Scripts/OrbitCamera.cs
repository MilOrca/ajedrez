﻿using UnityEngine;
using System.Collections;

public class OrbitCamera : MonoBehaviour {

	Vector3 center = new Vector3(0, 0, 0);
	Vector3 up = new Vector3(0, 1, 0);

	// Update is called once per frame
	void Update () {
		transform.LookAt (center);
		transform.RotateAround (center, up, Time.deltaTime * 10);
	}
}
