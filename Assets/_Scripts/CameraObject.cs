﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;


public class CameraObject : MonoBehaviour
{
	// First Postion To Flip To in 3D mode
	readonly Vector3 position1 = new Vector3 (0, 8, -5);
	readonly Vector3 rotation1 = new Vector3 (60, 0, 0);
	// Second Postion To Flip To in 3D mode
	readonly Vector3 position2 = new  Vector3 (0, 8, 5);
	readonly Vector3 rotation2 = new Vector3 (60, 180, 0);
	// Top Position in 2D Mode
	readonly Vector3 positionTop = new Vector3(0, 8, 0);
	readonly Vector3 rotationTop1 = new Vector3(90, 0, 0);
	readonly Vector3 rotationTop2 = new Vector3 (90, 180, 0);

	Camera camera; // Main Camera
	public bool mode2D; // Flag to indicate 2D/3D View
	public bool flip;  // Flag to indicate Witch Side The Board is on
	public bool orbit; // Flag, active white the board is flipping
	private float acumAngle; // acumulate angle white roatating 

	// Side Panel, to be adjusted after view port
	public Image sidePanel;

	// Text of the switch Button ( 3D and 2D)
	public Text textSwitch;

 	void Start()
	{
		int defaultView = PlayerPrefs.GetInt ("DefaultView");

		camera = gameObject.GetComponent<Camera> ();
		flip = false;
		orbit = false;

		// Adjust 2D Mode
		mode2D = defaultView == 0;
		if (mode2D)
		{
			camera.orthographic = true;
			textSwitch.text = "3D";
			UpdateView ();
		}

		// Ajust View Port
		AdjustViewPort ();

	}

	void AdjustViewPort()
	{
		float width, height, viewWidth, viewX;
		width = (float)Screen.width;
		height = (float)Screen.height;

		// Calculate View Port 
		viewWidth = height / width;
		viewX = (width-height)/width;

		// Set View Port
		camera.rect = new Rect (viewX, 0, viewWidth, 1);

		// Adjust Side Panel
		Vector2 anchor = sidePanel.rectTransform.anchorMax;
		anchor.x = viewX;
		sidePanel.rectTransform.anchorMax = anchor;
	}

	public Ray GetRay(Vector3 point)
	{
		return camera.ScreenPointToRay (point);
	}

	public void SwitchCamera()
	{
		mode2D = !mode2D;

		if (mode2D) 
		{
			camera.orthographic = true;
			textSwitch.text = "3D";
		}
		else
		{
			camera.orthographic = false;
			textSwitch.text = "2D";
		}

		UpdateView ();
	}

	public void Flip()
	{
		flip = !flip;
		if (mode2D) 
		{
			UpdateView ();
		} 
		else
		{  // In Case Of 3D we Orbit The Camera
			orbit = true;
			acumAngle = 0.0f;
		}
	}

	// Set The Postion, Orientation of the camera based on the current state
	void UpdateView()
	{
		if (flip)
		{
			if (mode2D)
			{
				gameObject.transform.position = positionTop;
				gameObject.transform.rotation = Quaternion.Euler(rotationTop2);
			}
			else
			{
				gameObject.transform.position = position2;
				gameObject.transform.rotation = Quaternion.Euler(rotation2);
			}
		}
		else
		{
			if (mode2D)
			{
				gameObject.transform.position = positionTop;
				gameObject.transform.rotation = Quaternion.Euler(rotationTop1);
			}
			else
			{
				gameObject.transform.position = position1;
				gameObject.transform.rotation = Quaternion.Euler(rotation1);
			}
		}
	}

	void Update()
	{
		if (orbit)
		{
			// Get The transformation from camera
			var transform = camera.transform;
			// Get The Direction of the Rotation
			int dir = (flip) ? 1 : -1; 
			// Set camera LookAt and Position
			transform.LookAt (new Vector3 (0, 0, 0));
			float deltaAngle = 200*Time.deltaTime;
			transform.RotateAround (new Vector3 (0, 0, 0), new Vector3 (0, 1, 0), dir*deltaAngle);
			// Acumulate the Angle
			acumAngle += deltaAngle;
			// Check if we Made a half Rotation
			if (acumAngle >= 180)
			{
				//transform.rotation = Quaternion.Euler(new Vector3(60, (flip) ? 180 : 0, 0));
				orbit = false;
				UpdateView();
			}
		}
	}
}
