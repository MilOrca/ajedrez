﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Xml;

public enum State
{
	IDLE, PICKING, MOVING, MOVED, ENEMYMOVE, SOLVED, FLIPPING

}

public class BoardObject : MonoBehaviour 
{

	// UI Elements
	public MessageObject message;
	public GameObject buttonMenu;
	public GameObject playMenu;
	public GameObject panelMenu;
	public GameObject infoView;
	public Text playersColor;

	// Moves Scroll View 
	public Text movesView;
	float moveHeight;

	// Level Loading Variables
	public static int levelIndex;
	int levelCount;
	Level level;

	// Audio Source and clips
	AudioSource audioSource;
	public AudioClip correctSound;
	public AudioClip wrongSound;
	public AudioClip clapping;
	float volume;

	public Transform buttomLeft; // 3D Place Holder for the Buttom-Left Corner of the Board
	public Transform topRight;	// 3D Place Holder for the Top-Right Corner of the Board
	public CameraObject mainCamera;	// Pointer to the main Camera (RayCasting)
	public SquareObject squarePrefab; // The Chess Board Square Prefab
	public SquareObject[,] squares; // Holds The Instances of the Square Prefab
	public Board board;	// Holds the Chess Pieces and Logic.

	State state;		// The Current State of The Game
	bool locked; 	// Lock The Game While a Menu is active

	// Animation Values on MOVING State
	Vector3 originalPos; // The Original Postion of The Piece
	Vector3 direction; // The Direction of The Move
	float destance; // Destance to move the Piece
	SquareObject destination;  // The Destination of a Move (For Animation)

	Dictionary<string, GameObject> piecePrefabs; // Piece Prefabs

	float xstep, zstep, xoffset, zoffset; // For Calcultaing 3D Positions


	// Use this for initialization
	void Start ()
	{
		state = State.IDLE;
		locked = false;

		// Get the Selected Level
		var package = PackagesScene.packages [LevelsScene.packageIndex];
		levelCount = package.levels.Count;
		level = package.levels[levelIndex];

		// Set Message
		message.Show(new Message("", Color.white));

		// Clear The Scroll View of Moves 
		movesView.text = "";
		if (moveHeight == 0) 
		{
			moveHeight = movesView.rectTransform.sizeDelta.y;
		}
		else
		{
			float moveWidth = movesView.rectTransform.sizeDelta.x;
			movesView.rectTransform.sizeDelta = new Vector2(moveWidth, moveHeight);
		}

		// Desable Next, Previous Level from Menu
		buttonMenu.SetActive (true);
		playMenu.SetActive (false);
		panelMenu.SetActive (false);
		EnableNextPrevious (false);

		// Set Level Info in the InfoView
		Text text = infoView.transform.Find ("LevelInfo").gameObject
			.GetComponent<Text>();
		text.text = level.ToString ();

		// Get Auddio Source Component
		audioSource = GetComponent<AudioSource> ();
		audioSource.Stop();
		volume = PlayerPrefs.GetFloat ("SoundVolume");

		// Calculate Offsets and Postions
		CalculateScenePositions ();
		// Load Prefabs
		LoadPiecePrefabs ();

		// Load The Level from Notations 
		board = NotationParser.Load (level.Play);

		// Indicate Players Color
		ShowPlayersColor ();

		// Draw Loaded Peices 
		DrawPieces ();

		// Adjust The Front Side of The Board Based on the Player
		if ((board.playerColor == PieceColor.BLACK && !mainCamera.flip)
		    || (board.playerColor == PieceColor.WHITE && mainCamera.flip))
		{
			mainCamera.Flip();
			FlipBoard();
		}

	}

	void ShowPlayersColor()
	{
		playersColor.text = "You're Playing With ";
		if (board.playerColor == PieceColor.WHITE)
		{
			playersColor.text += "White";
			playersColor.color = Color.white;
		}
		else
		{
			playersColor.text += "Black";
			playersColor.color = Color.black;
		}
	}

	public void Reset()
	{
		board = NotationParser.Load (level.Play);
		ClearPieces ();
		ClearSquares ();
		DrawPieces ();
		state = State.IDLE;
		locked = false;
		message.Show (new Message());
		movesView.text = "";
		audioSource.Stop();
		buttonMenu.SetActive (true);
		playMenu.SetActive (false);
		panelMenu.SetActive (false);
		infoView.SetActive (false);
	}

	public void SwitchMode()
	{
		ClearPieces ();
		DrawPieces ();
	}

	public void FlipBoard()
	{
		if (mainCamera.mode2D) {
			float angleY = (mainCamera.flip) ? 180 : 0;
			for (int i=0; i<8; i++) {
				for (int j=0; j<8; j++) {
					if (squares [i, j].piece != null) {
						squares [i, j].piece.transform.rotation = Quaternion.Euler (90, angleY, 0);
					}
				}
			}
		}
	}

	public void Hint()
	{
		if (state == State.IDLE || state == State.PICKING) {
			Vector2 hint = board.Hint ();
			if (hint.x != -1) {
				var square = squares [(int)hint.y, (int)hint.x];
				Process (square);
			}
		}
		CloseMenu ();
	}

	// Show Menu
	public void ShowhMenu()
	{
		if (state != State.SOLVED)
		{
			panelMenu.SetActive (true);
			buttonMenu.SetActive(false);
			locked = true;
		}
	}

	// Close Menu
	public void CloseMenu()
	{
		panelMenu.SetActive (false);
		buttonMenu.SetActive (true);
		locked = false;
	}

	// Show/Hide Info
	public void ShowInfo()
	{
		infoView.SetActive (true);
		panelMenu.SetActive (false);
	}

	public void CloseInfo()
	{
		infoView.SetActive (false);
		buttonMenu.SetActive (true);
		locked = false;
	}

	// Load Next Level
	public void LoadNextLevel ()
	{
		if (levelIndex + 1 < levelCount)
		{
			levelIndex++;
			ClearSquares();
			ClearPieces();
			Start();
		}
	}

	// Load Previous Level
	public void LoadPreviousLevel()
	{
		if (levelIndex - 1 >= 0)
		{
			levelIndex--;
			ClearPieces();
			ClearSquares();
			Start();
		}
	}

	// Back to Level Selection Selection
	public void BackToLevelScene()
	{
		Application.LoadLevel (2);
	}

    void EnableNextPrevious(bool enable)
	{
		var next = playMenu.transform.Find ("ButtonNext")
			.gameObject;
		var previous = playMenu.transform.Find ("ButtonPrevious")
			.gameObject;

		next.SetActive( (levelIndex == levelCount - 1) ? false : enable );
		previous.SetActive( (levelIndex == 0) ? false : enable );
	}

	// Calculates The 3D Postions 
	 void CalculateScenePositions()
	{
		// Get X,Z Step
		xstep = (topRight.position.x - buttomLeft.position.x)/8;
		zstep = (topRight.position.z - buttomLeft.position.z)/8;
		
		// Get X,Z Offset
		xoffset = buttomLeft.transform.position.x + xstep/2;
		zoffset = buttomLeft.transform.position.z + zstep/2;
		
		// Instantiate The Prefab Sqaure
		squares = new SquareObject[8,8]; 
		for (int x=0; x<8; x++) 
		{
			for (int z=0; z<8; z++)
			{
				AddSquare(x,z);
			}
		}
	}

	 void AddSquare(int x, int z)
	{
		// Calcualte position and scale
		Vector3 pos = new Vector3(x*xstep + xoffset, 0.0f, z*zstep + zoffset);
		Vector3 scale = new Vector3(xstep, 0.5f, zstep);
		// Create a new Sqaure and Instantiate it
		SquareObject square = Instantiate<SquareObject>(squarePrefab);
		// Set Parametres of the Square
		// Set The Postion in the scene
		square.transform.position = pos;
		// Set the scale in the scene
		square.transform.localScale = scale;
		// Set Position on the board
		square.position = new Vector2 (x, z);
		//Add The Square
		squares [z, x] = square;
	}

	 void LoadPiecePrefabs()
	{
		piecePrefabs = new Dictionary<string, GameObject> ();

		string[] prefabs = {"whitepawn", "whiterook", "whitebishop", "whiteknight",
						"blackpawn", "blackrook", "blackbishop", "blackknight",
						"whitequeen", "blackqueen", "whiteking", "blackking"
			};

		foreach (string name in prefabs) 
		{
			GameObject pieceObject = Resources.Load ("Pieces/"+name) as GameObject;
			piecePrefabs.Add(name, pieceObject);
			GameObject piece2DObject = Resources.Load ("Pieces2D/"+name+"2D") as GameObject;
			piecePrefabs.Add(name+"2D", piece2DObject);
		}
	}

	 void DrawPieces()
	{
		var pieces = board.GetPieces ();
		foreach (Piece piece in pieces) 
		{
			AddPiece(piece);
		}
	}

	 void AddPiece(Piece piece)
	{
		// Get Piece Name
		string name = piece.ToString();
		if (mainCamera.mode2D)
		{
			name += "2D";
		}
		// Get The Correspondant Prefab
		GameObject pieceObject = piecePrefabs[name];
		// Get Piece 3D Position
		SquareObject square = squares [(int)piece.position.y, (int)piece.position.x];
		// Set 3D Position
		pieceObject.transform.position = square.transform.position + new Vector3(0, 0.1f, 0);
		// Set Orientation
		if (mainCamera.mode2D)
		{
			float angleY = (mainCamera.flip) ? 180 : 0;
			pieceObject.transform.rotation = Quaternion.Euler (90, angleY, 0);
		}
		// Instantiate the Peice and Assotiate it to the Square
		square.piece = Instantiate (pieceObject);

	}

	void ClearPieces()
	{
		for(int i=0; i<8; i++)
		{
			for(int j=0; j<8; j++)
			{
				if (squares[i,j].piece != null)
				{
					GameObject.Destroy(squares[i,j].piece);
				}
			}
		}
	}

	 void ClearSquares()
	{
		for(int i=0; i<8; i++)
		{
			for(int j=0; j<8; j++)
			{
				squares[i,j].SetState(MoveType.NONE);
			}
		}
	}

	 void UpdateSquares(List<PossibleMove> moves)
	{

		foreach(PossibleMove move in moves)
		{
			// If There is a move
			if (move.type != MoveType.NONE)
			{
				// update The square State
				int i = (int)move.position.y;
				int j = (int)move.position.x;
				squares[i,j].SetState(move.type);
			}
		}
	}

	// Add a Move Notation ti Moves View
	void AddNotation(MoveInfo move)
	{
		if (board.currentColor == PieceColor.WHITE)
		{
			movesView.text +=  board.turn + "." + move.notation;
		}
		else
		{
			if (string.IsNullOrEmpty(movesView.text))
			{
				movesView.text = "1...";
			}
			movesView.text += " " + move.notation + "\n";
			movesView.rectTransform.offsetMin = new Vector2(0,0);
			movesView.rectTransform.offsetMax = new Vector2(0, moveHeight*board.turn);
		}
	}

	// Update is called once per frame
	void Update () 
	{
		// Check Mouse Input
	 	if (Input.GetMouseButtonDown (0) && !locked) 
		{
			ProcessInput();
		}

		if (state == State.MOVING)
		{
			UpdateMove();

		}
		else if (state == State.ENEMYMOVE)
		{
			ProcessEnemyMove();
		}
		else if (state == State.MOVED)
		{
			ProcessMoved();
		}
	}

	// Process Input
	void ProcessInput()
	{
		// Trace a Ray
		RaycastHit hit;
		Ray ray = mainCamera.GetRay(Input.mousePosition);
		// Check for a Hit
		if (Physics.Raycast(ray, out hit)) {
			// Get hit Object 
			GameObject obj = hit.collider.gameObject;
			// Check if It's a SquareObject
			if (obj.tag.Equals("Square"))
			{
				// Get The Selected Square
				Vector2 position = obj.GetComponent<SquareObject>().position;
				// ----- To Fix an Instantiating a Square ----
				var square = squares[(int)position.y, (int)position.x];
				// Process The Sqaure
				Process(square);
			}
		}
	}

	// Process The Playes Action
	void Process(SquareObject square)
	{
		switch (state)
		{
			case State.PICKING:
				ProcessPicking(square);
				break;
			case State.IDLE:
				ProcessIdle(square);
				break;
			default:
				break;
		}

	}

	// Process The Playes Action
	void ProcessIdle(SquareObject square)
	{
		// Check if There is possible moves
		var moves = board.GetPossibleMoves (square.position);

		if (moves != null && moves.Count > 0) 
		{
			// Mark Selected Piece
			square.SetState (MoveType.SELECTED);
			// Show Possible Moves
			UpdateSquares(moves);
			// Go To Picking State
			state = State.PICKING;
		}
	}

	// Process The Playes Action
	void ProcessPicking(SquareObject square)
	{
		// If Another Piece was Selected
		var moves = board.GetPossibleMoves (square.position);
		if (moves != null && moves.Count > 0 ) 
		{
			// Clear The Sqaures
			ClearSquares ();
			// Mark Selected Piece
			square.SetState(MoveType.SELECTED);
			// Show Possible Moves
			UpdateSquares (moves);
			return;
		} 

		// If The a Sqaure With Move or Capture was Selected
		if (square.state != MoveType.NONE) 
		{
			MoveInfo move = board.CheckMove(square.position);
			if (move.type != MoveType.NONE)
			{
				message.Show(new Message("Correct!", Color.green), true);
				audioSource.PlayOneShot(correctSound, volume);
				AddNotation(move);
				InitiateMove(move);
			}
			else
			{
				message.Show(new Message("Wrong Move! Try Again ...", Color.red), true);
				audioSource.PlayOneShot(wrongSound, volume);
			}
		}
	}

	// Process The Opponents Re-Action 
	void ProcessEnemyMove()
	{
		MoveInfo move = board.GetNextMove ();
		AddNotation(move);
		InitiateMove (move);
	}

	// Initiates The Selected Move
	void InitiateMove(MoveInfo move)
	{
		// Get Destination Sqaure, and The Selected Peice
		destination = squares [(int)move.to.y, (int)move.to.x];
		GameObject piece = squares [(int)move.from.y, (int)move.from.x].piece;
		// Store the Original Postion
		originalPos = piece.transform.position;

		if (move.captured.x != -1) 
		{
			// In Case of a capture remove the piece from Board
			GameObject.Destroy(squares[(int)move.captured.y,
			                           (int)move.captured.x].piece);
		}

		// Attach Selected Peice to the Destination Square
		destination.piece = piece;
		// DeAttach Selected Peice from The Original Square
		squares [(int)move.from.y, (int)move.from.x].piece = null;

		// Get Destination from Square, and the Actual 3D Position of The Piece
		Vector3 dest = destination.transform.position;
		Vector3 from = destination.piece.transform.position;
		// Calculate The Direction and Distance
		Vector3 dir = dest - from;
		destance = dir.magnitude;
		direction =  Vector3.Normalize(dir);

		// Clear The Possible Moves from Board
		ClearSquares ();
		// Set Next State
		state = State.MOVING;
	}

	// Animate The Moving Piece
	void UpdateMove()
	{
		// Update Actual Position of the Peice
		Vector3 currentPos = destination.piece.transform.position;
		currentPos += direction*10*Time.deltaTime;
		// CAlculate Current Destance
		float currentDest = Vector3.Magnitude (currentPos - originalPos);
		if (currentDest > destance )
		{
			// The Peice is within The Destination Square
			destination.piece.transform.position = destination.transform.position;
			destination = null;
			// Indicate that the move has finished
			state = State.MOVED;
		}
		else
		{
			// Apply The Updated Position
			destination.piece.transform.position = currentPos;
		}
	}

	// Process The Aftermath of a move
	void ProcessMoved()
	{
		if (board.CastleMove)
		{
			// Check if The current move is a Castling
			InitiateMove(board.RookMove);
			board.CastleMove = false;
		}
		else if (board.IsSolved ())
		{
			// Check if the Board is solved
			message.Show(new Message("Solved!", Color.white));
			audioSource.PlayOneShot(clapping, volume);
			// Show Menu
			locked = true;
			playMenu.SetActive(true);

			state = State.SOLVED;
			// Enable Next,Previous Buttons
			EnableNextPrevious(true);

		}
		else
		{
			if (board.Promotion)
			{
				// Check if it's a Promoted Pawn
				// Re-Draw The Board To Show The Promoted To Piece
				ClearPieces();
				DrawPieces();
				board.Promotion = false;
			}

			// Switch Color Turns
			board.SwitchTurn();
			// Set Next State based by Turns Color
			state = (board.IsPlayersTrun()) ? State.IDLE : State.ENEMYMOVE;
		}
	}

}
