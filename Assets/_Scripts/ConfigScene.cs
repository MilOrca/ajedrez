﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ConfigScene : MonoBehaviour {

	public Slider volumeSlider;
	public Slider defaultViewSlider;
	public InputField serverURLInput;

	public string defaultServer = "http://donkikochan.uab.cat/02/package1.xml";

	void Start()
	{
		float volume = PlayerPrefs.GetFloat ("SoundVolume");
		int defaultView = PlayerPrefs.GetInt ("DefaultView");
		string serverURL = PlayerPrefs.GetString ("ServerURL");

		volumeSlider.value = volume;
		defaultViewSlider.value = (float)defaultView;
		serverURLInput.text = string.IsNullOrEmpty(serverURL) ? defaultServer : serverURL;
	}

	public void OnBackClick()
	{
		float volume = volumeSlider.value;
		float defaultView = defaultViewSlider.value;
		string serverURL = serverURLInput.text;

		PlayerPrefs.SetFloat ("SoundVolume", volume);
		PlayerPrefs.SetInt("DefaultView", (defaultView > 0.5f) ? 1 : 0);
		PlayerPrefs.SetString ("ServerURL", serverURL);

		Application.LoadLevel (0);
	}
}
