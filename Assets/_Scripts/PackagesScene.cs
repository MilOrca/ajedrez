﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System;

public class PackagesScene : MonoBehaviour {

	public Image contentPanel;
	public Image packageView;

	public GameObject imageLoading;
	public GameObject panelTryAgain;

	public static bool online;
	public static List<Package> packages;
	int onlineIndex;

	void Start()
	{
		imageLoading.SetActive(true);
		panelTryAgain.SetActive (false);

		if (online)
		{
			string serverURL = PlayerPrefs.GetString("ServerURL");
			try
			{
				packages = PackageLoader.Download(serverURL);
			}
			catch (Exception)
			{
				packages = null;
			}
		}
		else
		{
			packages = PackageLoader.LoadLocal("Packages/local");
		}

		if (packages == null)
		{
			panelTryAgain.SetActive(true);
		}
		else
		{
			ShowPackages();
		}

		imageLoading.SetActive(false);
	}

	void ShowPackages()
	{
		int packageCount = packages.Count;
		
		RectTransform rectContainer = contentPanel.GetComponent<RectTransform> ();
		RectTransform rectPackageView = packageView.GetComponent<RectTransform> ();
		
		float height = rectPackageView.rect.height;
		float containerHeight = packageCount * height;
		
		contentPanel.rectTransform.offsetMin = 
			new Vector2 (rectContainer.offsetMin.x, -containerHeight);
		contentPanel.rectTransform.offsetMax = 
			new Vector2 (rectContainer.offsetMax.x, 0);
		
		for(int i= 0; i<packageCount; i++)
		{
			var package = packages[i];
			float y = containerHeight / 2 - height*(i+1);
			AddPackageView(i, package, y, height);
		}

	}

	void AddPackageView(int index, Package package, float y, float height)
	{
		var packageV = Instantiate<Image> (packageView);
		packageV.transform.SetParent (contentPanel.transform);
		packageV.name = string.Format("{0}", index);
		
		var text = packageV.GetComponentInChildren<Text> ();
		text.text = package.name;
		
		var rect = packageV.GetComponent<RectTransform> ();
		
		rect.offsetMin = new Vector2 (0,y);
		rect.offsetMax = new Vector2(0, y + height);
	}

	public void OnBackClick()
	{
		Application.LoadLevel (0);
	}

	// A Package Button Clicked
	public void onPackageClick(Image packageView)
	{
		Debug.Log ("clicked : " + packageView.name);

		imageLoading.SetActive (true);
		LevelsScene.packageIndex = int.Parse (packageView.name);
		Application.LoadLevel (2);
	}
}
