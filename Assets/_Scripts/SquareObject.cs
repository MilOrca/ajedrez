﻿using UnityEngine;
using System.Collections;

public class SquareObject : MonoBehaviour {

	public Vector2 position; // Relative Position on Board
	public MoveType state; // State of the Square
	public GameObject piece; // Chess Piece on this pocision or null

	// Set the State, and Chnage The Color of the Square
	public void SetState(MoveType state)
	{
		this.state = state;
		Material mat = gameObject.GetComponent<MeshRenderer> ().material;
		Color color = new Color();
		switch (state) 
		{
			case MoveType.SELECTED:
				color = Color.yellow;
				color.a = 0.5f;
				break;
			case MoveType.MOVE:
				color = Color.green;
				color.a = 0.5f;
				break;
			case MoveType.CAPTURE:
				color = Color.red;
				color.a = 0.5f;
				break;
			default:
				color.a = 0.0f;
				break;
		}
		mat.color = color;
	}
}
