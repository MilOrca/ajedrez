﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Xml;

public class BoardObjectDemo : MonoBehaviour 
{	
	public Transform buttomLeft; // 3D Place Holder for the Buttom-Left Corner of the Board
	public Transform topRight;	// 3D Place Holder for the Top-Right Corner of the Board
	public SquareObject squarePrefab; // The Chess Board Square Prefab
	public SquareObject[,] squares; // Holds The Instances of the Square Prefab
	public Board board;	// Holds the Chess Pieces and Logic.
	
	State state;		// The Current State of The Game
	
	// Animation Values on MOVING State
	Vector3 originalPos; // The Original Postion of The Piece
	Vector3 direction; // The Direction of The Move
	float destance; // Destance to move the Piece
	SquareObject destination;  // The Destination of a Move (For Animation)
	
	Dictionary<string, GameObject> piecePrefabs; // Piece Prefabs
	
	float xstep, zstep, xoffset, zoffset; // For Calcultaing 3D Positions

	string play;

	// Use this for initialization
	void Awake ()
	{
		state = State.ENEMYMOVE;

		//Calculate Offsets and Postions
		CalculateScenePositions ();
		// Load Prefabs
		LoadPiecePrefabs ();
		
		// Load The Level from Demo Notations 
		TextAsset text = Resources.Load<TextAsset> ("Packages/demo");
		play = text.text;
		board = NotationParser.Load (play);
		
		// Draw Loaded Peices 
		DrawPieces ();
	}
	
	public void Reset()
	{
		board = NotationParser.Load (play);
		ClearPieces ();
		ClearSquares ();
		DrawPieces ();
		state = State.ENEMYMOVE;
	}
	
	// Calculates The 3D Postions 
	void CalculateScenePositions()
	{
		// Get X,Z Step
		xstep = (topRight.position.x - buttomLeft.position.x)/8;
		zstep = (topRight.position.z - buttomLeft.position.z)/8;
		
		// Get X,Z Offset
		xoffset = buttomLeft.transform.position.x + xstep/2;
		zoffset = buttomLeft.transform.position.z + zstep/2;
		
		// Instantiate The Prefab Sqaure
		squares = new SquareObject[8,8]; 
		for (int x=0; x<8; x++) 
		{
			for (int z=0; z<8; z++)
			{
				AddSquare(x,z);
			}
		}
	}
	
	void AddSquare(int x, int z)
	{
		// Calcualte position and scale
		Vector3 pos = new Vector3(x*xstep + xoffset, 0.0f, z*zstep + zoffset);
		Vector3 scale = new Vector3(xstep, 0.5f, zstep);
		// Create a new Sqaure and Instantiate it
		SquareObject square = Instantiate<SquareObject>(squarePrefab);
		// Set Parametres of the Square
		// Set The Postion in the scene
		square.transform.position = pos;
		// Set the scale in the scene
		square.transform.localScale = scale;
		// Set Position on the board
		square.position = new Vector2 (x, z);
		//Add The Square
		squares [z, x] = square;
	}
	
	void LoadPiecePrefabs()
	{
		piecePrefabs = new Dictionary<string, GameObject> ();
		
		string[] prefabs = {"whitepawn", "whiterook", "whitebishop", "whiteknight",
			"blackpawn", "blackrook", "blackbishop", "blackknight",
			"whitequeen", "blackqueen", "whiteking", "blackking"
		};
		
		foreach (string name in prefabs) 
		{
			GameObject pieceObject = Resources.Load ("Pieces/"+name) as GameObject;
			piecePrefabs.Add(name, pieceObject);
		}
	}
	
	void DrawPieces()
	{
		var pieces = board.GetPieces ();
		foreach (Piece piece in pieces) 
		{
			AddPiece(piece);
		}
	}
	
	void AddPiece(Piece piece)
	{
		// Get Piece Name
		string name = piece.ToString();

		// Get The Correspondant Prefab
		GameObject pieceObject = piecePrefabs[name];
		// Get Piece 3D Position
		SquareObject square = squares [(int)piece.position.y, (int)piece.position.x];
		// Set 3D Position
		pieceObject.transform.position = square.transform.position;

		// Instantiate the Peice and Assotiate it to the Square
		square.piece = Instantiate (pieceObject);
		
	}
	
	void ClearPieces()
	{
		for(int i=0; i<8; i++)
		{
			for(int j=0; j<8; j++)
			{
				if (squares[i,j].piece != null)
				{
					GameObject.Destroy(squares[i,j].piece);
				}
			}
		}
	}
	
	void ClearSquares()
	{
		for(int i=0; i<8; i++)
		{
			for(int j=0; j<8; j++)
			{
				squares[i,j].SetState(MoveType.NONE);
			}
		}
	}

	// Update is called once per frame
	void Update () 
	{
		if (state == State.MOVING)
		{
			UpdateMove();
			
		}
		else if (state == State.ENEMYMOVE)
		{
			ProcessEnemyMove();
		}
		else if (state == State.MOVED)
		{
			ProcessMoved();
		}
	}

	// Process The Opponents Re-Action 
	void ProcessEnemyMove()
	{
		MoveInfo move = board.GetNextMove ();
		InitiateMove (move);
	}
	
	// Initiates The Selected Move
	void InitiateMove(MoveInfo move)
	{
		// Get Destination Sqaure, and The Selected Peice
		destination = squares [(int)move.to.y, (int)move.to.x];
		GameObject piece = squares [(int)move.from.y, (int)move.from.x].piece;
		// Store the Original Postion
		originalPos = piece.transform.position;
		
		if (move.captured.x != -1) 
		{
			// In Case of a capture remove the piece from Board
			GameObject.Destroy(squares[(int)move.captured.y,
			                           (int)move.captured.x].piece);
		}
		
		// Attach Selected Peice to the Destination Square
		destination.piece = piece;
		// DeAttach Selected Peice from The Original Square
		squares [(int)move.from.y, (int)move.from.x].piece = null;
		
		// Get Destination from Square, and the Actual 3D Position of The Piece
		Vector3 dest = destination.transform.position;
		Vector3 from = destination.piece.transform.position;
		// Calculate The Direction and Distance
		Vector3 dir = dest - from;
		destance = dir.magnitude;
		direction =  Vector3.Normalize(dir);
		
		// Clear The Possible Moves from Board
		ClearSquares ();
		// Set Next State
		state = State.MOVING;
	}
	
	// Animate The Moving Piece
	void UpdateMove()
	{
		// Update Actual Position of the Peice
		Vector3 currentPos = destination.piece.transform.position;
		currentPos += 5*direction*Time.deltaTime;
		// CAlculate Current Destance
		float currentDest = Vector3.Magnitude (currentPos - originalPos);
		if (currentDest > destance )
		{
			// The Peice is within The Destination Square
			destination.piece.transform.position = destination.transform.position;
			destination = null;
			// Indicate that the move has finished
			state = State.MOVED;
		}
		else
		{
			// Apply The Updated Position
			destination.piece.transform.position = currentPos;
		}
	}
	
	// Process The Aftermath of a move
	void ProcessMoved()
	{
		if (board.CastleMove)
		{
			// Check if The current move is a Castling
			InitiateMove(board.RookMove);
			board.CastleMove = false;
		}
		else if (board.IsSolved ())
		{
			// Check if the Board is solved
			Reset();
		}
		else
		{
			if (board.Promotion)
			{
				// Check if it's a Promoted Pawn
				// Re-Draw The Board To Show The Promoted To Piece
				ClearPieces();
				DrawPieces();
				board.Promotion = false;
			}
			
			// Switch Color Turns
			board.SwitchTurn();
			// Set Next State based by Turns Color
			state =  State.ENEMYMOVE;
		}
	}
	
}
