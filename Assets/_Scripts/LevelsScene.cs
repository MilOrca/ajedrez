﻿using UnityEngine;
using UnityEngine.UI;

using System.Collections;

public class LevelsScene : MonoBehaviour {

	public static int packageIndex;

	public Text title; // Title Bar
	public Image contentPanel; // Scroll View content
	public Image levelView;  // Level View Template

	public GameObject loadingImage;

	// Use this for initialization
	void Start () {

		loadingImage.SetActive (true);

		var selectedPackage = PackagesScene.packages [packageIndex];
		var levels = selectedPackage.levels;
		int levelCount = levels.Count;

		title.text = selectedPackage.name;

		RectTransform rectContainer = contentPanel.GetComponent<RectTransform> ();
		RectTransform rectLevelView = levelView.GetComponent<RectTransform> ();

		float height = rectLevelView.rect.height;
		float containerHeight = levelCount * height;

		contentPanel.rectTransform.offsetMin = new Vector2 (rectContainer.offsetMin.x, -containerHeight);
		contentPanel.rectTransform.offsetMax = new Vector2 (rectContainer.offsetMax.x, 0);

		for(int i= 0; i<levelCount; i++)
		{
			var level = levels[i];
			float y = containerHeight / 2 - height*(i+1);
			AddLevelView(i, level, y, height);
		}

		loadingImage.SetActive (false);
	}

	void AddLevelView(int index, Level level, float y, float height)
	{
		var levelV = Instantiate<Image> (levelView);
		levelV.transform.SetParent (contentPanel.transform);
		levelV.name = string.Format("{0}", index);

		var texts = levelV.GetComponentsInChildren<Text> ();
		texts[0].text = string.Format("{0} - {1}", level.White, level.Black);
		texts[1].text = string.Format("{0}, {1}", level.Date, level.Site);

		var rect = levelV.GetComponent<RectTransform> ();

		rect.offsetMin = new Vector2 (0,y);
		rect.offsetMax = new Vector2(0, y + height);
	}

	// A Level Button Clicked
	public void onLevelClick(Image levelView)
	{
		Debug.Log ("clicked : " + levelView.name);

		loadingImage.SetActive (true);

		BoardObject.levelIndex = int.Parse (levelView.name);
		Application.LoadLevel (3);

		loadingImage.SetActive (false);
	}

	// Back Button Clicked
	public void onBackClick()
	{
		Application.LoadLevel (1);
	}

}
