﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class MainScene : MonoBehaviour {

	public GameObject panelPlay;
	public GameObject imageLoading;

	void Start()
	{
		imageLoading.SetActive (false);
		panelPlay.SetActive (false);
	}

	public void OnStartClick()
	{
		bool active = !panelPlay.activeSelf;
		panelPlay.SetActive (active);
	}

	public void OnLocalPlayClick()
	{
		PackagesScene.online = false;
		imageLoading.SetActive (true);
		Application.LoadLevel (1);
	}

	public void OnOnlinePlayClick()
	{
		PackagesScene.online = true;
		imageLoading.SetActive (true);
		Application.LoadLevel (1);
	}

	public void OnCongfigClick()
	{
		Application.LoadLevel (4);
	}

	public void OnExitClick()
	{
		Application.Quit ();
	}
}
