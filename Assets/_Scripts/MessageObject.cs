﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public struct Message
{
	public string text;
	public Color color;

	public Message(string text, Color color)
	{
		this.text = text;
		this.color = color;
	}
}

public enum ShowState
{
	NONE, SHOW, FADE
}

public class MessageObject : MonoBehaviour {

	public float maxTimer = 1.0f; 
	float timer;

	ShowState state;

	public void Show(Message messgae)
	{
		Text text = GetComponent<Text> ();
		text.text = messgae.text;
		text.color = messgae.color;
		state = ShowState.NONE;
	}

	public void Show(Message messgae, bool fade)
	{
		Show (messgae);
		if (fade)
		{
			state = ShowState.SHOW;
			timer = maxTimer;
		}
		else
		{
			state = ShowState.NONE;
		}
	}

	void Start()
	{
		state = ShowState.NONE;
	}

	void Update()
	{
		if (state == ShowState.SHOW) {
			timer -= Time.deltaTime;
			if (timer < 0) {
				state = ShowState.FADE;
			}
		}
		else if (state == ShowState.FADE)
		{
			Text text = GetComponent<Text> ();
			Color color = text.color;
			color.a -= Time.deltaTime;
			text.color = color;
			if (color.a <= 0)
			{
				state = ShowState.NONE;
			}
		}
	}
}
